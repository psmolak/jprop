package com.smolak.pawel.reader;

import com.smolak.pawel.graph.IGraph;

import java.io.File;
import java.io.IOException;

public abstract class GraphReader<T> {
    abstract IGraph<T> getGraph() throws IOException;
}
