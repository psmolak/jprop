package com.smolak.pawel.reader;

import com.smolak.pawel.graph.Digraph;
import com.smolak.pawel.graph.IGraph;

import java.io.*;
import java.util.Scanner;

public class SimpleDigraphReader extends GraphReader<Integer> {
    private BufferedReader reader;

    public SimpleDigraphReader(InputStreamReader reader) throws FileNotFoundException {
        this.reader = new BufferedReader(reader);
    }

    @Override
    public IGraph<Integer> getGraph() throws IOException {
        String line = this.reader.readLine();

        if (line == null)
            return null;

        Scanner scr = new Scanner(line).useDelimiter(" ");

        Integer verticies = scr.hasNextInt() ? scr.nextInt() : null;
        Integer edges = scr.hasNextInt() ? scr.nextInt() : null;

        if (verticies == null || edges == null)
            throw new RuntimeException("Error while reading graph.");

        IGraph<Integer> graph = new Digraph<>();

        for (int i = 0; i < verticies; i++) {
            graph.addVertex(i);
        }

        for (int i = 0; i < edges; i++) {
            Integer source = scr.hasNextInt() ? scr.nextInt() : null;
            Integer destination = scr.hasNextInt() ? scr.nextInt() : null;

            if (source == null || destination == null)
                throw new RuntimeException("Error while reading " + (i+1) + " edge definition.");

            graph.addEdge(source, destination);
        }

        return graph;
    }
}
