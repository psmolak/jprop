package com.smolak.pawel.cli;

import com.beust.jcommander.JCommander;
import com.smolak.pawel.graph.IGraph;
import com.smolak.pawel.graph.algorithms.*;
import com.smolak.pawel.reader.SimpleDigraphReader;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class Application {
    private Options opts;

    public Application(String[] argv) {
        this.opts = new Options();

        JCommander.newBuilder()
                .addObject(this.opts)
                .build()
                .parse(argv);
    }

    public void run() throws IOException {
        List<InputStreamReader> readers = new ArrayList<InputStreamReader>();

        for (String filename : this.opts.files) {
            readers.add(new InputStreamReader(new FileInputStream(filename)));
        }

        // jeśli nie podano nazw plików, użyj stdin
        if (readers.isEmpty())
            readers.add(new InputStreamReader(System.in));

        for (InputStreamReader reader : readers) {
            SimpleDigraphReader dig = new SimpleDigraphReader(reader);
            IGraph<Integer> graph;

            do {
                graph = dig.getGraph();
                if (graph == null) break;

                processGraph(graph);
                System.out.println();
            } while (true);
        }
    }

    private void processGraph(IGraph<Integer> graph) {
        Map<Integer, Map<Integer, Integer>> distances = null;
        Boolean is_connected = new Connectivity<Integer>(graph).isConnected();

        if (this.opts.all) {
            this.opts.degree = true;
            this.opts.radius = true;
            this.opts.diameter  = true;
            this.opts.central = true;
            this.opts.peripheral = true;
            this.opts.pseudo_peripheral = true;
            this.opts.connected = true;
            this.opts.tree = true;
            this.opts.regular = true;
        }

        if (this.opts.radius
                || this.opts.diameter
                || this.opts.connected
                || this.opts.central
                || this.opts.peripheral
                || this.opts.pseudo_peripheral )
            distances = new ShortestPaths<Integer>(graph).getPathLengthsFromAll();

        if (this.opts.degree) {
            Degree<Integer> dgr = new Degree<Integer>(graph);
            System.out.println("Stopień: " + dgr.getDegree());
        }

        if (this.opts.regular) {
            Regularity<Integer> regular = new Regularity<>(graph);
            System.out.println("Graf " + (regular.isRegular() ? "" : " nie") + " jest regularny");
        }

        if (this.opts.connected) {
            Connectivity<Integer> conn = new Connectivity<Integer>(graph, distances);
            System.out.println("Graf " + (conn.isConnected() ? "" : " nie") + " jest spójny");
        }

        if (this.opts.radius) {
            Radius<Integer> radius = new Radius<Integer>(graph, distances);
            System.out.println("Prmień: " + (is_connected ? radius.getRadius() : "Tylko dla spójnych grafów"));
        }

        if (this.opts.diameter) {
            Diameter<Integer> diameter = new Diameter<Integer>(graph, distances);
            System.out.println("Średnica: " + (is_connected ? diameter.getDiameter() : "Tylko dla spójnych grafów"));
        }

        if (this.opts.central) {
            Verticies<Integer> vertAlgs = new Verticies<Integer>(graph, distances);
            System.out.println("Wierzchołki centralne: " + vertAlgs.getCenterVerticies());
        }

        if (this.opts.peripheral) {
            Verticies<Integer> vertAlgs = new Verticies<Integer>(graph, distances);
            System.out.println("Wierzchołki obwodowe: " + vertAlgs.getPeripheralVertices());
        }

        if (this.opts.pseudo_peripheral) {
            Verticies<Integer> vertAlgs = new Verticies<Integer>(graph, distances);
            System.out.println("Wierzchołki pseudo-obwodowe: " + vertAlgs.getPseudoPeripheralVertices());
        }
    }
}
