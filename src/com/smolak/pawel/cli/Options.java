package com.smolak.pawel.cli;

import com.beust.jcommander.Parameter;

import java.util.ArrayList;
import java.util.List;

class Options {
    @Parameter(description = "Main input files")
    List<String> files;

    @Parameter(names = { "-a", "--all" }, description = "Enable all graph property flags")
    boolean all;

    @Parameter(names = { "--format" }, description = "Graph description format")
    String format = "digraph6t";

    @Parameter(names = { "-n", "--degree" }, description = "Wypisuje stopień grafu")
    boolean degree;

    @Parameter(names = { "-r", "--radius" }, description = "Wypisuje promień grafu")
    boolean radius;

    @Parameter(names = { "-d", "--diameter" }, description = "Wypisuje średnicę grafu")
    boolean diameter;

    @Parameter(names = { "-c", "--central" }, description = "Wypisuje wierzchołki centralne")
    boolean central;

    @Parameter(names = { "-p", "--peripheral" }, description = "Wypisuje wierzchołki obwodowe")
    boolean peripheral;

    @Parameter(names = { "-h", "--pseudo-peripheral" }, description = "Wypisuje wierzchołki pseudo-obwodowe")
    boolean pseudo_peripheral;

    @Parameter(names = { "-C", "--is-connected" }, description = "Wypisuje czy graf jest spójny")
    boolean connected;

    @Parameter(names = { "-T", "--is-tree" }, description = "Wypisuje czy graf jest drzewem")
    boolean tree;

    @Parameter(names = { "-R", "--is-regular" }, description = "Wypisuje czy graf jest regularny")
    boolean regular;
}
