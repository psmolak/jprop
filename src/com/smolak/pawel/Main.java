package com.smolak.pawel;

import com.smolak.pawel.cli.Application;
import com.smolak.pawel.graph.BFSIterator;
import com.smolak.pawel.graph.Digraph;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) throws IOException {
        Application cli = new Application(args);
        cli.run();
    }
}
