package com.smolak.pawel.graph;

import java.util.List;
import java.util.Set;

public interface IGraph<T> extends Iterable<T> {
    void addVertex(T value);

    void removeVertex(T value);

    void addEdge(T source, T destination);

    void removeEdge(T source, T destination);

    List<T> adjentTo(T vertex);

    DFSIterator<T> getDFSIterator(T start);

    BFSIterator<T> getBFSIterator(T start);

    Set<T> getVerticies();
}
