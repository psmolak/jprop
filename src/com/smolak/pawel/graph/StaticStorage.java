package com.smolak.pawel.graph;

import java.util.List;
import java.util.Vector;

public class StaticStorage<T> extends Storage<T> {
    public StaticStorage() {
        super();
    }

    @Override
    public List<T> initAdjents() {
        return new Vector<T>();
    }
}
