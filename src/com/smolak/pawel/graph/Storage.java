package com.smolak.pawel.graph;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public abstract class Storage<T> {
    private Map<T, List<T>> storage;

    public abstract List<T> initAdjents();

    public Storage() {
        this.storage = new HashMap<T, List<T>>();
    }

    public List<T> add(T value) {
        if (!this.storage.containsKey(value)) {
            this.storage.put(value, initAdjents());
        }

        return this.storage.get(value);
    }

    public void connect(T source, T destination) {
        List<T> adjents = add(source);

        if (!adjents.contains(destination)) {
            adjents.add(destination);
        }
    }

    public void removeConnection(T source, T destination) {
        List<T> elements = storage.get(source);

        if (elements != null) {
            elements.remove(destination);
        }

        storage.put(source, elements);
    }

    public void remove(T value) {
        storage.put(value, null);

        for (T key : storage.keySet()) {
            storage.get(key).remove(value);
        }
    }

    public List<T> adjentTo(T value) {
        return this.storage.getOrDefault(value, null);
    }

    public Set<T> keys() {
        return this.storage.keySet();
    }
}
