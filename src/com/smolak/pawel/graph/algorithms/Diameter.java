package com.smolak.pawel.graph.algorithms;

import com.smolak.pawel.graph.IGraph;

import java.util.Collections;
import java.util.Map;

public class Diameter<T> {
    private Map<T, Map<T, Integer>> distances;
    private IGraph<T> graph;

    // precomputed paths constructor
    public Diameter(IGraph<T> graph, Map<T, Map<T, Integer>> distances) {
        this.graph = graph;
        this.distances = distances;
    }

    public Diameter(IGraph<T> graph) {
        this(graph, new ShortestPaths<T>(graph).getPathLengthsFromAll());
    }

    public Integer getDiameter() {
        Map<T, Integer> eccents = new Verticies<T>(this.graph, this.distances).getEccentricities();
        return Collections.max(eccents.values());
    }
}
