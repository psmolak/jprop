package com.smolak.pawel.graph.algorithms;

import com.smolak.pawel.graph.IGraph;

import java.util.*;

public class Verticies<T> {
    private IGraph<T> graph;
    private Map<T, Map<T, Integer>> distances;

    public Verticies(IGraph<T> graph, Map<T, Map<T, Integer>> distances) {
        this.graph = graph;
        this.distances = distances;
    }

    public Verticies(IGraph<T> graph) {
        this(graph, new ShortestPaths<T>(graph).getPathLengthsFromAll());
    }

    public Map<T, Integer> getEccentricities() {
        Map<T, Integer> eccs = new HashMap<T, Integer>();

        for (T vertex : this.graph) {
            Map<T, Integer> paths = this.distances.get(vertex);
            eccs.put(vertex, Collections.max(paths.values()));
        }

        return eccs;
    }

    public Map.Entry<T, Integer> getMaxEccent(Map<T, Integer> eccents) {
        Map.Entry<T, Integer> max = null;

        for (Map.Entry<T, Integer> eccent : eccents.entrySet()) {
            if (max != null && eccent.getValue() > max.getValue()) {
                max = eccent;
            } else {
                max = eccent;
            }
        }

        return max;
    }

    public List<T> getCenterVerticies() {
        Set<T> centrals = new HashSet<T>();
        Integer radius = new Radius<T>(this.graph, this.distances).getRadius();

        for (Map.Entry<T, Integer> eccent : getEccentricities().entrySet()) {
            if (eccent.getValue().equals(radius))
                centrals.add(eccent.getKey());
        }

        return new ArrayList<T>(centrals);
    }

    public List<T> getPeripheralVertices() {
        Set<T> peripherals = new HashSet<T>();
        Integer diameter = new Diameter<T>(this.graph, this.distances).getDiameter();

        for (Map.Entry<T, Integer> eccent : getEccentricities().entrySet()) {
            if (eccent.getValue().equals(diameter))
                peripherals.add(eccent.getKey());
        }

        return new ArrayList<T>(peripherals);
    }

    public List<T> getPseudoPeripheralVertices() {
        Set<T> pseudo = new HashSet<T>();
        Map<T, Integer> eccents = getEccentricities();

        for (T v : this.graph) {
            for (T u : this.graph) {

                Integer uvdist = this.distances.get(u).get(v);
                Integer eu = eccents.get(u);
                Integer ev = eccents.get(v);

                if (uvdist != null && eu != null && ev != null
                        && uvdist.equals(eu) && eu.equals(ev))
                    pseudo.add(v);

            }
        }

        return new ArrayList<T>(pseudo);
    }
}
