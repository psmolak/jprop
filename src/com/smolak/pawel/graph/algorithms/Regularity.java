package com.smolak.pawel.graph.algorithms;

import com.smolak.pawel.graph.IGraph;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class Regularity<T> {
    private IGraph<T> graph;

    public Regularity(IGraph<T> graph) {
        this.graph = graph;
    }

    public boolean isRegular() {
        HashMap<T, Integer> counters = new HashMap<>();

        for (T vertex : graph)
            counters.put(vertex, 0);

        for (T vertex : graph) {
            for (T neighbour : graph.adjentTo(vertex)) {
                counters.put(vertex, counters.get(vertex) + 1);
                counters.put(neighbour, counters.get(neighbour) + 1);
            }
        }

        Set<Integer> degrees = new HashSet<Integer>(counters.values());
        return degrees.size() == 1;
    }
}
