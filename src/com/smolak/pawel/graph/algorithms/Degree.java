package com.smolak.pawel.graph.algorithms;

import com.smolak.pawel.graph.IGraph;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class Degree<T> {
    private IGraph<T> graph;

    public Degree(IGraph<T> graph) {
        this.graph = graph;
    }

    public Integer getDegree() {
        HashMap<T, Integer> counters = new HashMap<>();
        Integer max = 0;

        for (T vertex : graph)
            counters.put(vertex, 0);

        for (T vertex : graph) {
            for (T neighbour : graph.adjentTo(vertex)) {
                // zwiększ licznik krawędzi wychodzących dla vertex
                counters.put(vertex, counters.get(vertex) + 1);

                // zwiększ licznik krawędzi wchodzących do neighbour
                counters.put(neighbour, counters.get(neighbour) + 1);
            }
        }

        for (Map.Entry<T, Integer> entry : counters.entrySet() ) {
            if (entry.getValue() > max)
                max = entry.getValue();
        }

        return max;
    }
}
