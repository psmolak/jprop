package com.smolak.pawel.graph.algorithms;

import com.smolak.pawel.graph.BFSIterator;
import com.smolak.pawel.graph.IGraph;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

public class Connectivity<T> {
    private IGraph<T> graph;
    private Map<T, Map<T, Integer>> distances;

    public Connectivity(IGraph<T> graph, Map<T, Map<T, Integer>> distances) {
        this.graph = graph;
        this.distances = distances;
    }

    public Connectivity(IGraph<T> graph) {
        this(graph, null);
    }

    public boolean isConnected() {
        Iterator<T> iterator = this.graph.getVerticies().iterator();
        Set<T> verticies = this.graph.getVerticies();

        if (iterator.hasNext()) {
            T vertex = iterator.next();
            Set<T> visited = new HashSet<T>();

            BFSIterator<T> bfs = this.graph.getBFSIterator(vertex);
            while (bfs.hasNext()) {
                if (!verticies.contains(bfs.next()))
                    return false;
            }
            return true;
        }
        return true;
    }
}
