package com.smolak.pawel.graph.algorithms;

import com.smolak.pawel.graph.BFSIterator;
import com.smolak.pawel.graph.Digraph;
import com.smolak.pawel.graph.IGraph;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class ShortestPaths<T> {
    private IGraph<T> graph;

    public ShortestPaths(IGraph<T> graph) {
        this.graph = graph;
    }

    public Map<T, Integer> getPathLengthsFromVertex(T start) {
        Map<T, Integer> distances = new HashMap<>();

        BFSIterator<T> iterator = graph.getBFSIterator(start);
        Integer distance = 0;

        while (iterator.hasNext()) {
            T vertex = iterator.next();

            distances.putIfAbsent(vertex, distance);

            for (T neighbour : graph.adjentTo(vertex)) {
                final Integer local = distance;
                distances.computeIfAbsent(neighbour, k -> local + 1);
            }

            distance++;
        }

        return distances;
    }

    public Map<T, Map<T, Integer>> getPathLengthsFromAll() {
        Map<T, Map<T, Integer>> distances = new HashMap<>();

        for (T vertex : graph) {
            distances.put(vertex, getPathLengthsFromVertex(vertex));
        }

        return distances;
    }
}
