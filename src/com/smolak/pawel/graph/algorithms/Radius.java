package com.smolak.pawel.graph.algorithms;

import com.smolak.pawel.graph.IGraph;

import java.util.Collections;
import java.util.Map;

public class Radius<T> {
    private Map<T, Map<T, Integer>> distances;
    private IGraph<T> graph;

    // precomputed paths constructor
    public Radius(IGraph<T> graph, Map<T, Map<T, Integer>> distances) {
        this.graph = graph;
        this.distances = distances;
    }

    public Radius(IGraph<T> graph) {
        this(graph, new ShortestPaths<T>(graph).getPathLengthsFromAll());
    }

    public Integer getRadius() {
        Map<T, Integer> eccents = new Verticies<T>(this.graph, this.distances).getEccentricities();
        return Collections.min(eccents.values());
    }
}
