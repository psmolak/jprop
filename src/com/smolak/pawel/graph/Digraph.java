package com.smolak.pawel.graph;

import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class Digraph<T> implements IGraph<T> {
    private Storage<T> edges;

    public Digraph(Storage<T> storage) {
        this.edges = storage;
    }

    public Digraph() {
        this(new DynamicStorage<T>());
    }

    @Override
    public void addVertex(T value) {
        this.edges.add(value);
    }

    @Override
    public void removeVertex(T value) {
        this.edges.remove(value);
    }

    @Override
    public void removeEdge(T source, T destination) {
        this.edges.removeConnection(source, destination);
    }

    @Override
    public void addEdge(T source, T destination) {
        this.addVertex(source);
        this.addVertex(destination);

        this.edges.connect(source, destination);
    }

    @Override
    public List<T> adjentTo(T vertex) {
        return this.edges.adjentTo(vertex);
    }

    @Override
    public DFSIterator<T> getDFSIterator(T start) {
        return new DFSIterator<T>(this, start);
    }

    @Override
    public BFSIterator<T> getBFSIterator(T start) {
        return new BFSIterator<T>(this, start);
    }

    @Override
    public Iterator<T> iterator() {
        return this.edges.keys().iterator();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();

        for (T vertex : this) {
            sb.append(vertex).append( " -> ( ");

            for (T neighbour : adjentTo(vertex)) {
                sb.append(neighbour).append(" ");
            }

            sb.append(")\n");
        }

        return sb.toString();
    }

    @Override
    public Set<T> getVerticies() {
        return this.edges.keys();
    }
}
