package com.smolak.pawel.graph;

import java.util.*;

public class DFSIterator<T> implements Iterator<T> {
    private Digraph<T> digraph;
    private Set<T> visited;
    private Stack<T> stack;

    public DFSIterator(Digraph<T> digraph, T start) {
        this.visited = new HashSet<T>();
        this.stack = new Stack<T>();
        this.digraph = digraph;

        stack.push(start);
    }

    @Override
    public boolean hasNext() {
        return !stack.isEmpty();
    }

    @Override
    public T next() {
        T top = stack.pop();
        visited.add(top);

        for (T adjent : digraph.adjentTo(top)) {
            if (!(visited.contains(adjent) || stack.contains(adjent))) {
                stack.push(adjent);
            }
        }

        return top;
    }
}
