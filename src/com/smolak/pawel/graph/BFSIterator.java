package com.smolak.pawel.graph;

import java.util.*;

public class BFSIterator<T> implements Iterator<T> {
    private Set<T> visited;
    private Queue<T> queue;
    private IGraph<T> digraph;

    public BFSIterator(IGraph<T> digraph, T start) {
        this.visited = new HashSet<T>();
        this.queue = new ArrayDeque<T>();
        this.digraph = digraph;

        queue.add(start);
    }

    @Override
    public boolean hasNext() {
        return !queue.isEmpty();
    }

    @Override
    public T next() {
        T top = queue.remove();
        visited.add(top);

        for (T adjent : digraph.adjentTo(top)) {
            if (!(visited.contains(adjent) || queue.contains(adjent))) {
                queue.add(adjent);
            }
        }

        return top;
    }
}
