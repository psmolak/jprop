package com.smolak.pawel.graph;

import java.util.LinkedList;
import java.util.List;

public class DynamicStorage<T> extends Storage<T> {
    public DynamicStorage() {
        super();
    }

    @Override
    public List<T> initAdjents() {
        return new LinkedList<T>();
    }
}
