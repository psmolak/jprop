package com.smolak.pawel.graph;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class DynamicStorageTest {
    private DynamicStorage<Integer> intStorage = new DynamicStorage<Integer>();
    private DynamicStorage<String> stringStorage = new DynamicStorage<String>();

    @org.junit.jupiter.api.Test
    void addIntegersWithoutDuplicates() {
        intStorage.add(1);
        intStorage.add(2);
        intStorage.add(3);

        assertTrue(intStorage.keys().size() == 3);

        List<Integer> orig = Arrays.asList(1, 2, 3);
        assertTrue(intStorage.keys().containsAll(orig));
    }

    @org.junit.jupiter.api.Test
    void addIntegersWithDuplicates() {
        intStorage.add(1);
        intStorage.add(2);
        intStorage.add(3);
        intStorage.add(1);
        intStorage.add(2);
        intStorage.add(3);

        assertTrue(intStorage.keys().size() == 3);

        List<Integer> orig = Arrays.asList(1, 2, 3);
        assertTrue(intStorage.keys().containsAll(orig));
    }

    @org.junit.jupiter.api.Test
    void connectIntegersWithoutDuplicates() {
        intStorage.connect(1, 1);
        intStorage.connect(1, 2);
        intStorage.connect(1, 3);
        intStorage.connect(1, 4);
        intStorage.connect(1, 5);

        intStorage.connect(2, 1);
        intStorage.connect(2, 2);
        intStorage.connect(2, 3);
        intStorage.connect(2, 4);
        intStorage.connect(2, 5);

        assertTrue(intStorage.adjentTo(1).size() == 5);
        assertTrue(intStorage.adjentTo(2).size() == 5);

        List<Integer> orig = Arrays.asList(1, 2, 3, 4, 5);
        assertTrue(intStorage.adjentTo(1).containsAll(orig));
        assertTrue(intStorage.adjentTo(2).containsAll(orig));
    }

    @org.junit.jupiter.api.Test
    void connectIntegersWithDuplicates() {
        intStorage.connect(1, 1);
        intStorage.connect(1, 2);
        intStorage.connect(1, 3);
        intStorage.connect(1, 1);
        intStorage.connect(1, 2);
        intStorage.connect(1, 3);
        intStorage.connect(1, 4);
        intStorage.connect(1, 5);

        intStorage.connect(2, 1);
        intStorage.connect(2, 2);
        intStorage.connect(2, 3);
        intStorage.connect(2, 1);
        intStorage.connect(2, 2);
        intStorage.connect(2, 3);
        intStorage.connect(2, 4);
        intStorage.connect(2, 5);

        assertTrue(intStorage.adjentTo(1).size() == 5);
        assertTrue(intStorage.adjentTo(2).size() == 5);

        List<Integer> orig = Arrays.asList(1, 2, 3, 4, 5);
        assertTrue(intStorage.adjentTo(1).containsAll(orig));
        assertTrue(intStorage.adjentTo(2).containsAll(orig));
    }

    @org.junit.jupiter.api.Test
    void addStringsWithoutDuplicates() {
        stringStorage.add("one");
        stringStorage.add("two");
        stringStorage.add("three");

        assertTrue(stringStorage.keys().size() == 3);

        List<String> orig = Arrays.asList("one", "two", "three");
        assertTrue(stringStorage.keys().containsAll(orig));
    }

    @org.junit.jupiter.api.Test
    void addStringsWithDuplicates() {
        stringStorage.add("one");
        stringStorage.add("two");
        stringStorage.add("three");
        stringStorage.add("one");
        stringStorage.add("two");
        stringStorage.add("three");

        assertTrue(stringStorage.keys().size() == 3);

        List<String> orig = Arrays.asList("one", "two", "three");
        assertTrue(stringStorage.keys().containsAll(orig));
    }

    @org.junit.jupiter.api.Test
    void connectStringsWithoutDuplicates() {
        stringStorage.connect("one", "one");
        stringStorage.connect("one", "two");
        stringStorage.connect("one", "three");
        stringStorage.connect("one", "four");
        stringStorage.connect("one", "five");

        stringStorage.connect("two", "one");
        stringStorage.connect("two", "two");
        stringStorage.connect("two", "three");
        stringStorage.connect("two", "four");
        stringStorage.connect("two", "five");

        assertTrue(stringStorage.adjentTo("one").size() == 5);
        assertTrue(stringStorage.adjentTo("two").size() == 5);

        List<String> orig = Arrays.asList("one", "two", "three", "four", "five");
        assertTrue(stringStorage.adjentTo("one").containsAll(orig));
        assertTrue(stringStorage.adjentTo("two").containsAll(orig));
    }

    @org.junit.jupiter.api.Test
    void connectStringsWithDuplicates() {
        stringStorage.connect("one", "one");
        stringStorage.connect("one", "two");
        stringStorage.connect("one", "one");
        stringStorage.connect("one", "two");
        stringStorage.connect("one", "three");
        stringStorage.connect("one", "four");
        stringStorage.connect("one", "five");

        stringStorage.connect("two", "one");
        stringStorage.connect("two", "two");
        stringStorage.connect("two", "one");
        stringStorage.connect("two", "two");
        stringStorage.connect("two", "three");
        stringStorage.connect("two", "four");
        stringStorage.connect("two", "five");

        assertTrue(stringStorage.adjentTo("one").size() == 5);
        assertTrue(stringStorage.adjentTo("two").size() == 5);

        List<String> orig = Arrays.asList("one", "two", "three", "four", "five");
        assertTrue(stringStorage.adjentTo("one").containsAll(orig));
        assertTrue(stringStorage.adjentTo("two").containsAll(orig));
    }
}