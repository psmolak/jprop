package com.smolak.pawel.graph;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class DigraphTest {
    Digraph<Integer> digraph = new Digraph<Integer>();

    @Test
    void addEdgeNonExistentVertex() {
        digraph.addEdge(1, 2);
        digraph.addEdge(3, 4);

        assertTrue(digraph.getVerticies().size() == 4);
    }

    @Test
    void adjentToEmpty() {
        digraph.addVertex(1);
        assertTrue(digraph.adjentTo(1).size() == 0);
    }

    @Test
    void adjentToNonExistent() {
        assertTrue(digraph.adjentTo(1) == null);
    }

    @Test
    void adjentToNonEmpty() {
        digraph.addEdge(1, 1);
        digraph.addEdge(1, 2);
        digraph.addEdge(1, 3);

        digraph.addVertex(2);
        digraph.addEdge(2, 1);
        digraph.addEdge(2, 2);
        digraph.addEdge(2, 3);


        List<Integer> orig = Arrays.asList(1, 2, 3);
        assertTrue(digraph.adjentTo(1).containsAll(orig));
        assertTrue(digraph.adjentTo(2).containsAll(orig));
    }

    @Test
    void basicIteratorInteger() {
        digraph.addVertex(1);
        digraph.addVertex(2);
        digraph.addVertex(3);
        digraph.addVertex(4);
        digraph.addVertex(5);

        List<Integer> orig = Arrays.asList(1, 2, 3, 4, 5);
        List<Integer> iter = new ArrayList<Integer>();

        for (Integer vertex : digraph) {
            iter.add(vertex);
        }

        assertTrue(iter.size() == 5);
        assertTrue(iter.containsAll(orig));
    }
}