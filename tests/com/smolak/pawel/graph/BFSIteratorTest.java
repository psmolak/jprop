package com.smolak.pawel.graph;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Created by psmolak on 18.06.17.
 */
class BFSIteratorTest {
    Digraph<Integer> digraph = new Digraph<Integer>();

    @Test
    void bfsTraversalEmpty() {
        BFSIterator<Integer> it = digraph.getBFSIterator(1);
        assertThrows(NullPointerException.class, ()-> {it.next(); });
    }

    @Test
    void bfsTraaversalIsolated() {
        digraph.addVertex(8);
        digraph.addEdge(1, 2);

        BFSIterator<Integer> it = digraph.getBFSIterator(8);
        List<Integer> list = new ArrayList<Integer>();
        List<Integer> orig = Arrays.asList(8);

        while (it.hasNext()) {
            list.add(it.next());
        }

        assertEquals(list, orig);
    }

    @Test
    void bfsTraversalMixed() {
        digraph.addEdge(1, 2);
        digraph.addEdge(1, 3);
        digraph.addEdge(3, 2);
        digraph.addEdge(2, 4);
        digraph.addEdge(4, 6);
        digraph.addEdge(4, 5);
        digraph.addEdge(5, 2);
        digraph.addEdge(6, 7);
        digraph.addEdge(6, 5);
        digraph.addVertex(8);

        BFSIterator<Integer> it = digraph.getBFSIterator(1);
        List<Integer> list = new ArrayList<Integer>();
        List<Integer> orig = Arrays.asList(1, 2, 3, 4, 6, 5, 7);

        while (it.hasNext()) {
            list.add(it.next());
        }

        assertEquals(list, orig);
    }

}